# README #

Contents are the java class HOQC_MS.java that can read and write data to an MsAccess file and MATLAB code that uses this class.

The actual work is done by java package UCanAccess. I wrote HOQC_MS to make the functionality of UCanAccess available in MATLAB.

Also added are the jar-files needed to run the MATLAB code.
The file HOQC_MS.m contains an 'example of use' and copy right information.
HOQC_MS needs Java version 1.7 or later.

This software builds on

+ UCanAccess (http://sourceforge.net/projects/ucanaccess/) by Marco Amadei (his program Example.java inspired this software)
+ Jackcess (http://jackcess.sourceforge.net/) by Health Market Science 	(see remark)
+ HSQLDB (http://hsqldb.org/) by The hsql Development Group 

Remark: a small patch was applied to the latest snapshot version of Jackcess to avoid a warning during the initial opening of a database.
The patch has been applied to IndexData.java originally located in \src\main\java\com\healthmarketscience\jackcess\impl
The two sections that are changed are marked with the phrase HOQC. The patched file is added to this repository in the patch directory