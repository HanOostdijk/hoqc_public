classdef HOQC_MS
%HOQC_MS is a MATLAB class to handle MsAccess objects

    %{
Version:
HOQC_MS version 0.31 Copyright (c) 2014 Han Oostdijk (HOQC)
   www.hanoostdijk.nl

 Builds on UCanAccess (http://sourceforge.net/projects/ucanaccess/) by Marco Amadei
   (his program Example.java inspired this software)
 UCanAccess builds on:
   Jackcess (http://jackcess.sourceforge.net/) by Health Market Science		
   HSQLDB (http://hsqldb.org/) by The hsql Development Group

Copy rights:
HOQC_MS.m is dependent on the file fsearch.m 
HOQC_MS.m is dependent on java class hoqc.HOQC_MS 
hoqc.HOQC_MS is depended on UCanAccess 
    Copyright (c) 2012 Marco Amadei
    (http://sourceforge.net/projects/ucanaccess/) 
For details about copy rights of these products see  
HOQC_MS   : .\copyright_HOQC_MS.txt
fsearch.m : see fsearch.m
ucanaccess: .\myJava\libs\copyright_ucanaccess.txt

Limited testing done with:
R2014b (64-bit) Java Version: Java 1.7.0_11-b21     : okay
R2014a (64-bit) Java Version: Java 1.7.0_11-b21     : okay
R2013a (32-bit) Java Version: Java 1.6.0_17-b04     : does not work 

Known issues:
It is not clear if more than one database can be open
at the same time. Try this at your own risk.

Prerequisites:
The software builds on UCanAccess, Jackcess and HSQLDB and it is not clear 
to the author what are the prerequisites of this software for working
with MsAccess databases. MsAccess itself is not needed, but it could be 
that Microsoft Data Access Components is necessary.

Example of use:

    % point to map with this file
    cd('D:\data\matlab\hoqc_ms')

    % register all jar-files needed (they are in submap myJava)
    addjava('.\myJava')

    % create an object to communicate with Ms Access
    db = HOQC_MS() ;

    % set the name of the database (existing or non-existing)
    db.setdbasename('D:/data/matlab/hoqc_ms/hoqc1_aa1.mdb') ;

    % open the database
    db.opendb() ;

    % or combine the three previous calls in :
    % db = HOQC_MS('D:/data/matlab/hoqc_ms/hoqc1_aa1.mdb') ;

  	% check the version of the software
    db.version() ;

    % create a table with an automatic id
    % more examples on http://ucanaccess.sourceforge.net/site.html
    db.exec_sql('CREATE TABLE t1 (id COUNTER PRIMARY KEY, descr text(400))') ;

    % create two insert commands
    myInsert1 = 'INSERT INTO t1 (descr) VALUES(''Amsterdam'')' ;
    myInsert2 = 'INSERT INTO t1 (descr) VALUES(''Brussel'')' ;

    % execute the first insert command
    db.exec_sql(myInsert1)    

    % execute both insert commands
    db.exec_sql({myInsert1,myInsert2})

    % check if all three rows are present
    [data,names,types] = db.fetch_sql('select * from t1')

 	% is the count really 3 ?
    data = db.fetch_sql('select count(*) from t1')

    % other example (date in MATLAB in datenum format)
    db.exec_sql('CREATE TABLE t2 (id integer, entry_date date, descr text(400))')
    myInsert1 = ['INSERT INTO t2 (id, descr, entry_date) ' ...
                'VALUES(33,''Amsterdam'',#08/31/2014#)' ];
    db.exec_sql(myInsert1)
   	data = db.fetch_sql('select * from t2')

    % do something fancy (?)
    data = db.fetch_sql('select a.*, b.* from t1 a, t1 b')
    data = db.fetch_sql('select top 1 * from t1')

    % get information about tables/queries in database (all info)
    [data,names,types] = db.getMetaData()  

    % get information about tables/queries in database (short)
    table_info = db.getTableInfo()    

    % get information about table/query in the database (all info)
    [data,names,types] = db.getMetaData('t2')  

    % get information about table/query in database (short)
    col_info = db.getColumnInfo('t2')   

    % close the database 
    db.closedb() ;

    % any other methods implemented but not documented  here?
    methods(db)

    %}
    
    properties
        ex
    end
    
    methods
        function obj = HOQC_MS(varargin)
            obj.ex = hoqc.HOQC_MS(varargin{:}) ;
        end
        function version(obj)
            obj.ex.version()  ;
        end
        function setdbasename(obj,msa_filename)
            obj.ex.setdbasename(msa_filename) ;
        end
        function opendb(obj)
            obj.ex.opendb() ;
        end
        function closedb(obj)
            obj.ex.closedb() ;
        end
        function [data,varargout]=getMetaData(obj,varargin)
            nargoutchk(1,3);
            if nargin == 1
                x = obj.ex.getMetaData();
            else
                x = obj.ex.getMetaData(varargin{1}) ;
            end
            [data,varargout{1:nargout-1}] = convert(x) ;
        end
        function data=getTableInfo(obj)
            [data,names] = getMetaData(obj) ;
            data = [names(1:4)';data(:,1:4)] ;
        end
        function data=getColumnInfo(obj,tablename)
            [data,names]=getMetaData(obj,tablename);            
            data = [names([1:4,6:8])';data(:,[1:4,6:8])] ;
        end
        function [data,varargout]=fetch_sql(obj,sql)
            nargoutchk(1,3);
            x = obj.ex.fetch_sql(sql);
            [data,varargout{1:nargout-1}] = convert(x) ;
        end
        function exec_sql(obj,sql)
            obj.ex.exec_sql(sql);
        end
    end
end

function [ res,varargout] = convert(x)
% convert result of query
types = cell(x.get(0).toArray()) ;
colnames = cell(x.get(1).toArray()) ;
if nargout ==3 , varargout{2} = types ; end
if nargout >=2 , varargout{1} = colnames ; end

for i=2:x.size()-1
    col = myfetch_col(types{i-1},x.get(i)) ;
    if i ==2
        res = col ;
    else
        res = [res,col] ;
    end
end
end

function col = myfetch_col(type,xjava)
col = cell(xjava.toArray());                            % default processing
switch(type)
    case 'java.lang.String'
        % default processing is enough
    case 'java.lang.Double'
        % default processing is enough
    case 'java.lang.Integer'
        % default processing is enough
    case 'java.lang.Long'
        % default processing is enough
    case 'java.math.BigDecimal'
        col = arrayfun(@(x)double(x{1}),col,'UniformOutput',false) ;
    case {'java.sql.Timestamp' ,'java.sql.Date'}
         % default processing is enough
end
end

