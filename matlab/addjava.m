function addjava(topdir)
% add java jars and classes under topdir to java path
jars = fsearch('.jar', topdir, 'files');
for j = 1 : length(jars)
    javaaddpath(jars{j});
end
% add jars under topdir to java path
jars = fsearch('.class', topdir, 'files');
for j = 1 : length(jars)
    javaaddpath(jars{j});
end

end