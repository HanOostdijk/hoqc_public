/*
 original
https://github.com/andrew-nguyen/ucanaccess/blob/master/example/net/ucanaccess/example/Example.java
Copyright (c) 2012 Marco Amadei.
 modified Han Oostdijk
 */
package hoqc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import net.ucanaccess.jdbc.UcanaccessDriver;


public class HOQC_MS {

	public void version() {
		System.out.println(" ");
		System.out
				.println(" HOQC_MS version 0.31 Copyright (c) 2014 Han Oostdijk (HOQC www.hanoostdijk.nl)");
		System.out.println(" ");
		System.out
				.println(" Builds on UCanAccess (http://sourceforge.net/projects/ucanaccess/) by Marco Amadei");
		System.out
				.println("   (his program Example.java inspired this software)");
		System.out
				.println(" UCanAccess builds on:");
		System.out
				.println("   Jackcess (http://jackcess.sourceforge.net/) by Health Market Science");		
		System.out
				.println("   HSQLDB (http://hsqldb.org/) by The hsql Development Group");
		System.out.println(" ");
	}

	public ArrayList<ArrayList<Object>> fetch_sql(String sql)
			throws SQLException {
		Statement st = null;
		try {
			st = this.ucaConn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			return rs_2_al(rs);
		} finally {
			if (st != null)
				st.close();
		}
	}

	public void exec_sql(String sql) throws SQLException {
		Statement st = null;
		try {
			st = this.ucaConn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			st.execute(sql);
		} finally {
			if (st != null)
				st.close();
		}
	}
	
	public void exec_sql(String[] sqls) throws SQLException {
		Statement st = null;
		try {
			st = this.ucaConn.createStatement(ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			int i;
			for (i = 0; i < sqls.length; i++) {
				st.execute(sqls[i]);
			}

		} finally {
			if (st != null)
				st.close();
		}
	}

	public ArrayList<ArrayList<Object>> getMetaData() throws SQLException {
		ResultSet rs = null;
		try {
			DatabaseMetaData dbmd = ucaConn.getMetaData();
			// String[] types = { "TABLE" };
			String[] types = null;
			rs = dbmd.getTables(null, null, "%", types);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs_2_al(rs);
	}

	public ArrayList<ArrayList<Object>> getMetaData(String tablename)
			throws SQLException {
		ResultSet rs = null;
		try {

			DatabaseMetaData dbmd = ucaConn.getMetaData();
			String catalog = null;
			String schemaPattern = null;
			String columnNamePattern = null;
			rs = dbmd.getColumns(catalog, schemaPattern, tablename,
					columnNamePattern);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs_2_al(rs);
	}

	private static ArrayList<ArrayList<Object>> rs_2_al(ResultSet rs)
			throws SQLException {
		int nrcols = rs.getMetaData().getColumnCount();
		ArrayList<ArrayList<Object>> res = new ArrayList<ArrayList<Object>>(
				2 + nrcols);
		res.add(new ArrayList<Object>()); // will contain class names of columns
		res.add(new ArrayList<Object>()); // will contain names of columns
		boolean[] type_date =  new boolean[nrcols];
		for (int i = 0; i < nrcols; ++i) {
			String ccn = rs.getMetaData().getColumnClassName(i + 1);			
			int t = rs.getMetaData().getColumnType(i+1);
			if ((t ==91)|| (t==93))
				type_date[i] = true;
			else 
				type_date[i] = false;
			res.get(0).add(ccn); // add class name in first column
			String cn = rs.getMetaData().getColumnName(i + 1);
			res.get(1).add(cn); // add name in second column
			res.add(new ArrayList<Object>()); // initialize column data
		}
		while (rs.next()) {
			for (int i = 1; i <= nrcols; ++i) {
				Object o = rs.getObject(i);
				if (type_date[i-1] == true)  
					o = o.toString().substring(0,10) ; // only date part
				res.get(1 + i).add(o); // index 0/1 contains class names/names
										// of columns
			}
		}
		return res;
	} // end rs_2_al

	private static Connection getUcanaccessConnection(String pathNewDB)
			throws SQLException, IOException {
		String url = UcanaccessDriver.URL_PREFIX + pathNewDB
				+ ";newDatabaseVersion=V2003;memory=false;showschema=true";
		return DriverManager.getConnection(url, "", "");
	}

	public void setdbasename(String dbname) {
		this.dbasename = dbname;
	}

	public void opendb() throws ClassNotFoundException, SQLException {
		Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		try {
			this.ucaConn = getUcanaccessConnection(dbasename);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void closedb() throws SQLException {
		try {
			ucaConn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void HOQC_MS_common(String level) {
		java.util.Properties log4jProperties = new java.util.Properties();
		log4jProperties.setProperty("log4j.rootLogger",
				level + ", myConsoleAppender");
		log4jProperties.setProperty("log4j.appender.myConsoleAppender",
				"org.apache.log4j.ConsoleAppender");
		log4jProperties.setProperty("log4j.appender.myConsoleAppender.layout",
				"org.apache.log4j.PatternLayout");
		log4jProperties.setProperty(
				"log4j.appender.myConsoleAppender.layout.ConversionPattern",
				"%-5p %c %x - %m%n");
		org.apache.log4j.PropertyConfigurator.configure(log4jProperties);
//		this.logger1 =
//			    org.apache.log4j.Logger.getLogger("com.healthmarketscience.jackcess");
//		this.logger1.setLevel(org.apache.log4j.Level.ERROR) ;
//		this.logger2 =
//			    org.apache.log4j.Logger.getLogger("com.healthmarketscience.jackcess./impl.IndexData");
//		this.logger2.setLevel(org.apache.log4j.Level.ERROR) ;
	}
	
	public HOQC_MS() {
		HOQC_MS_common("ERROR");		
	}

	public HOQC_MS(String dbname) throws ClassNotFoundException, SQLException {
		try {
			HOQC_MS_common("ERROR");	
			this.dbasename = dbname;
			this.opendb();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public HOQC_MS(String dbname,String level) throws ClassNotFoundException, SQLException {
		try {
			HOQC_MS_common(level);	
			this.dbasename = dbname;
			this.opendb();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
//	private org.apache.log4j.Logger logger1;
//	private org.apache.log4j.Logger logger2;
	private String dbasename;
	private Connection ucaConn;
}